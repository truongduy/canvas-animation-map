const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.height = 500;
canvas.width = 500;
let speed = 0.2;

let persons = [];

let person = {
  x: canvas.width / 2,
  y: canvas.height - 10,
  dx: speed,
  dy: -speed,
  radius: 3,
  draw: function() {
    ctx.beginPath();
    ctx.fillStyle = '#230c33';
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
  }
}

function drawBoard1() {
  ctx.beginPath();
  ctx.rect(60, 100, 4, 20);
  ctx.rect(60, 120, 40, 4);
  ctx.rect(100, 120, 4, 40);
  ctx.rect(60, 100, 160, 4);
  ctx.rect(60, 250, 350, 4);
  ctx.rect(100, 160, 120, 4);
  ctx.rect(220, 100, 4, 64);
  ctx.fillStyle = '#230c33';
  ctx.fill();
  ctx.closePath();
}

let personsGen = generatePerson(person);
var random = 0;
var baseRad = Math.PI * 2 / 6;

function generatePerson(person) {
  const newPerson = Object.assign({}, person);
  var random = (Math.random() < .5 ? 1 : -1);

  newPerson.dx = Math.cos(random) / 4 * random;
  newPerson.dy = Math.sin(random) / 4 * random;

  persons.push(newPerson);

  return persons;
}

function play() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawBoard1();

  personsGen.forEach(function(item, index) {
    item.draw();
    collisionDetection(item);

    item.x += item.dx;
    item.y += item.dy;
    random += baseRad * (Math.random() < .5 ? 1 : -1);

    item.x += item.dx * Math.cos(random);
    item.y += item.dy * Math.sin(random);

    if ((item.x + item.radius + 10) > canvas.width || (item.x - 10) - item.radius < 0) {
      item.dx *= -1;
    }

    if ((item.y + item.radius + 10) > canvas.height || (item.y - 10) - item.radius < 0) {
      item.dy *= -1;
    }
  });

  requestAnimationFrame(play);
}

function collisionDetection(person) {
  var arrayLine = [
    {x: 60, y: 100, w: 160, h: 4},
    {x: 100, y: 120, w: 4, h: 40},
    {x: 60, y: 120, w: 40, h: 4},
    {x: 60, y: 250, w: 350, h: 4}
  ];

  arrayLine.forEach(function(line) {
    if (
      person.x >= line.x - 6 &&
      person.x <= line.x + 6 + line.w &&
      person.y >= line.y - 6 &&
      person.y <= line.y + 6 + line.h
    ) {
      person.dy *= -1;
    }
  });
}

play();

let btnAddPerson = document.querySelector('.btn-add-person');

btnAddPerson.addEventListener('click', function() {
  personsGen = generatePerson(person);
  console.log(persons);

  document.querySelector('.count-person').innerText = persons.length;
});
