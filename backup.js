const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.height = 500;
canvas.width = 500;
let speed = 2;

let person = {
  x: canvas.width / 2,
  y: canvas.height - 50,
  dx: speed + 0.05,
  dy: -speed + 0.05,
  radius: 3,
  draw: function() {
    ctx.beginPath();
    ctx.fillStyle = '#230c33';
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
  }
}

function play() {
  ctx.clearRect(0, 0, canvas.width, canvas.height)

  person.draw();
  drawBricks();
  collisionDetection();

  person.x += person.dx;
  person.y += person.dy;

  if (person.x + person.radius > canvas.width || person.x - person.radius < 0) {
    person.dx *= -1;
  }

  if (person.y + person.radius > canvas.height || person.y - person.radius < 0) {
    person.dy *= -1;
  }

  requestAnimationFrame(play);
}

let brickRowCount = 4;
let brickColumnCount = 2;
let brickWidth = 120;
let brickHeight = 2;
let brickPadding = 100;
let brickOffsetTop = 100;
let brickOffsetLeft = 80;

let bricks = [];

function generateBricks() {
  for (let c = 0; c < brickColumnCount; c++) {
    bricks[c] = [];
    for (let r = 0; r < brickRowCount; r++) {
      bricks[c][r] = {x: 0, y: 0, status: 1};
    }
  }
}

function drawBricks() {
  for (let c = 0; c < brickColumnCount; c++) {
    for (let r = 0; r < brickRowCount; r++) {
      if (bricks[c][r].status === 1) {
        let brickX = c * (brickWidth + brickPadding) + brickOffsetLeft;
        let brickY = r * (brickHeight + brickPadding) + brickOffsetTop;
        bricks[c][r].x = brickX;
        bricks[c][r].y = brickY;

        ctx.beginPath();
        ctx.rect(brickX, brickY, brickWidth, brickHeight);
        ctx.fillStyle = '#230c33';
        ctx.fill();
        ctx.closePath();
      }
    }
  }
}

function collisionDetection() {
  for (let c = 0; c < brickColumnCount; c++) {
    for (let r = 0; r < brickRowCount; r++) {
      let b = bricks[c][r];
      if (b.status === 1) {
        if (
          person.x >= b.x &&
          person.x <= b.x + brickWidth &&
          person.y >= b.y &&
          person.y <= b.y + brickHeight
        ) {
          person.dy *= -1;
          // b.status = 0;
        }
      }
    }
  }
}

generateBricks();
play();
